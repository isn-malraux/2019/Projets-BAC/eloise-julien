# Joueur d'échantillons à déclencheur clavier
L’objectif de ce projet est de réaliser un *sampler* possédant deux
octaves à déclencher avec le clavier textuel d'un ordinateur. Chaque
note correspondrait à une touche précise du clavier. L’utilisateur
aurait la possibilité de changer d’instruments (piano, guitare
électrique/acoustique, flûte à bec…). Il sera possible de jouer
plusieurs notes en même temps.

# Cahier des charges fonctionnel
## Listes exhaustives des éléments et contraintes
- Capacité du clavier à accepter l’activation de plusieurs touches en
  même temps
- Placement des notes sur le clavier de manière pratique et pertinente
- Capacité à faire tenir une note si on reste appuyer
- Support visuel non-fonctionnel (simple et purement esthétique)
- Menu accessible à tout moment pour changer d'instrument
- Touche associée à une note précise
- Création d'une banque de son
  - un dossier par instrument
  - un fichier par tonalité

## Technologies et méthodes
- système d'exploitation : `GNU/Linux`
- langage : `Python3`
- modules : `pygame`
- support d’échange : https://framagit.org
