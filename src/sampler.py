#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 12 14:43:09 2019

@author: utilisateur
"""

import pygame
from pygame.locals import KEYDOWN
from pygame.locals import KEYUP
from pygame.locals import QUIT
import os.path 
pygame.init()
pygame.mixer.init()
pygame.display.init()
event = pygame.event.poll()

piano_synth = {
        pygame.K_w: pygame.mixer.Sound("sounds/piano_synth/Do.1.wav"),
        pygame.K_s: pygame.mixer.Sound("sounds/piano_synth/Do#.1.wav"),
        pygame.K_x: pygame.mixer.Sound("sounds/piano_synth/Re.1.wav"),
        pygame.K_d: pygame.mixer.Sound("sounds/piano_synth/Re#.1.wav"),
        pygame.K_c: pygame.mixer.Sound("sounds/piano_synth/Mi.1.wav"),
        pygame.K_v: pygame.mixer.Sound("sounds/piano_synth/Fa.1.wav"),
        pygame.K_g: pygame.mixer.Sound("sounds/piano_synth/Fa#.1.wav"),
        pygame.K_n: pygame.mixer.Sound("sounds/piano_synth/La.1.wav"),
        pygame.K_j: pygame.mixer.Sound("sounds/piano_synth/La#.1.wav"),
        pygame.K_COMMA: pygame.mixer.Sound("sounds/piano_synth/Si.1.wav"),
        pygame.K_y: pygame.mixer.Sound("sounds/piano_synth/Do.2.wav"),
        pygame.K_7: pygame.mixer.Sound("sounds/piano_synth/Do#.2.wav"),
        pygame.K_u: pygame.mixer.Sound("sounds/piano_synth/Re.2.wav"),
        pygame.K_8: pygame.mixer.Sound("sounds/piano_synth/Re#.2.wav"),
        pygame.K_i: pygame.mixer.Sound("sounds/piano_synth/Mi.2.wav"),
        pygame.K_o: pygame.mixer.Sound("sounds/piano_synth/Fa.2.wav"),
        pygame.K_0: pygame.mixer.Sound("sounds/piano_synth/Fa#.2.wav"),
        #pygame.K_p: pygame.mixer.Sound("sounds/piano_synth/Sol.2.wav"),
        #pygame.K_RIGHTPAREN: pygame.mixer.Sound("sounds/piano_synth/Sol#.2.wav"),
        pygame.K_CARET: pygame.mixer.Sound("sounds/piano_synth/La.2.wav"),
        pygame.K_KP_EQUALS: pygame.mixer.Sound("sounds/piano_synth/La#.2.wav"),
        pygame.K_DOLLAR: pygame.mixer.Sound("sounds/piano_synth/Si.2.wav"),
}

guitare_acoustique = {
        pygame.K_w: pygame.mixer.Sound("sounds/guitare_acoustique/Do.1.wav"),
        pygame.K_s: pygame.mixer.Sound("sounds/guitare_acoustique/Do#.1.wav"),
        pygame.K_x: pygame.mixer.Sound("sounds/guitare_acoustique/Re.1.wav"),
        pygame.K_d: pygame.mixer.Sound("sounds/guitare_acoustique/Re#.1.wav"),
        pygame.K_c: pygame.mixer.Sound("sounds/guitare_acoustique/Mi.1.wav"),
        pygame.K_v: pygame.mixer.Sound("sounds/guitare_acoustique/Fa.1.wav"),
        pygame.K_g: pygame.mixer.Sound("sounds/guitare_acoustique/Fa#.1.wav"),
        pygame.K_b: pygame.mixer.Sound("sounds/guitare_acoustique/Sol.1.wav"),
        pygame.K_h: pygame.mixer.Sound("sounds/guitare_acoustique/Sol#.1.wav"),
        pygame.K_n: pygame.mixer.Sound("sounds/guitare_acoustique/La.1.wav"),
        pygame.K_j: pygame.mixer.Sound("sounds/guitare_acoustique/La#.1.wav"),
        pygame.K_COMMA: pygame.mixer.Sound("sounds/guitare_acoustique/Si.1.wav"),
        pygame.K_y: pygame.mixer.Sound("sounds/guitare_acoustique/Do.2.wav"),
        pygame.K_7: pygame.mixer.Sound("sounds/guitare_acoustique/Do#.2.wav"),
        pygame.K_u: pygame.mixer.Sound("sounds/guitare_acoustique/Re.2.wav"),
        pygame.K_8: pygame.mixer.Sound("sounds/guitare_acoustique/Re#.2.wav"),
        pygame.K_i: pygame.mixer.Sound("sounds/guitare_acoustique/Mi.2.wav"),
        pygame.K_o: pygame.mixer.Sound("sounds/guitare_acoustique/Fa.2.wav"),
        pygame.K_0: pygame.mixer.Sound("sounds/guitare_acoustique/Fa#.2.wav"),
        pygame.K_p: pygame.mixer.Sound("sounds/guitare_acoustique/Sol.2.wav"),
        pygame.K_RIGHTPAREN: pygame.mixer.Sound("sounds/guitare_acoustique/Sol#.2.wav"),
        pygame.K_CARET: pygame.mixer.Sound("sounds/guitare_acoustique/La.2.wav"),
        pygame.K_KP_EQUALS: pygame.mixer.Sound("sounds/guitare_acoustique/La#.2.wav"),
        pygame.K_DOLLAR: pygame.mixer.Sound("sounds/guitare_acoustique/Si.2.wav"),
}

guitare_electrique = {
        pygame.K_w: pygame.mixer.Sound("sounds/guitare_electrique/Do.1.wav"),
        pygame.K_s: pygame.mixer.Sound("sounds/guitare_electrique/Do#.1.wav"),
        pygame.K_x: pygame.mixer.Sound("sounds/guitare_electrique/Re.1.wav"),
        pygame.K_d: pygame.mixer.Sound("sounds/guitare_electrique/Re#.1.wav"),
        pygame.K_c: pygame.mixer.Sound("sounds/guitare_electrique/Mi.1.wav"),
        pygame.K_v: pygame.mixer.Sound("sounds/guitare_electrique/Fa.1.wav"),
        pygame.K_g: pygame.mixer.Sound("sounds/guitare_electrique/Fa#.1.wav"),
        pygame.K_b: pygame.mixer.Sound("sounds/guitare_electrique/Sol.1.wav"),
        pygame.K_h: pygame.mixer.Sound("sounds/guitare_electrique/Sol#.1.wav"),
        pygame.K_n: pygame.mixer.Sound("sounds/guitare_electrique/La.1.wav"),
        pygame.K_j: pygame.mixer.Sound("sounds/guitare_electrique/La#.1.wav"),
        pygame.K_COMMA: pygame.mixer.Sound("sounds/guitare_electrique/Si.1.wav"),
        pygame.K_y: pygame.mixer.Sound("sounds/guitare_electrique/Do.2.wav"),
        pygame.K_7: pygame.mixer.Sound("sounds/guitare_electrique/Do#.2.wav"),
        pygame.K_u: pygame.mixer.Sound("sounds/guitare_electrique/Re.2.wav"),
        pygame.K_8: pygame.mixer.Sound("sounds/guitare_electrique/Re#.2.wav"),
        pygame.K_i: pygame.mixer.Sound("sounds/guitare_electrique/Mi.2.wav"),
        pygame.K_o: pygame.mixer.Sound("sounds/guitare_electrique/Fa.2.wav"),
        pygame.K_0: pygame.mixer.Sound("sounds/guitare_electrique/Fa#.2.wav"),
        pygame.K_p: pygame.mixer.Sound("sounds/guitare_electrique/Sol.2.wav"),
        pygame.K_RIGHTPAREN: pygame.mixer.Sound("sounds/guitare_electrique/Sol#.2.wav"),
        pygame.K_CARET: pygame.mixer.Sound("sounds/guitare_electrique/La.2.wav"),
        pygame.K_KP_EQUALS: pygame.mixer.Sound("sounds/guitare_electrique/La#.2.wav"),
        pygame.K_DOLLAR: pygame.mixer.Sound("sounds/guitare_electrique/Si.2.wav"),
}
         
         
flute_a_bec = {
        pygame.K_w: pygame.mixer.Sound("sounds/flute_a_bec/Do.1.wav"),
        pygame.K_x: pygame.mixer.Sound("sounds/flute_a_bec/Re.1.wav"),
        pygame.K_c: pygame.mixer.Sound("sounds/flute_a_bec/Mi.1.wav"),
        pygame.K_v: pygame.mixer.Sound("sounds/flute_a_bec/Fa.1.wav"),
        pygame.K_b: pygame.mixer.Sound("sounds/flute_a_bec/Sol.1.wav"),
        pygame.K_n: pygame.mixer.Sound("sounds/flute_a_bec/La.1.wav"),
        pygame.K_COMMA: pygame.mixer.Sound("sounds/flute_a_bec/Si.1.wav"),
}

instruments = {
        pygame.K_F1: {
                "mapping": piano_synth,
                "nom": "Piano synthétique"},
        pygame.K_F2: {
                "mapping": guitare_acoustique,
                "nom": "Guitare acoustique"},
        pygame.K_F3: {
                "mapping": guitare_electrique,
                "nom": "Guitare électrique"},
        pygame.K_F4: {
                "mapping": flute_a_bec,
                "nom": "Flûte à bec"},
}

fenetre = pygame.display.set_mode((1000, 1000))
fond = pygame.image.load(os.path.join("/home/utilisateur/ISN/projet-bac-sampler/src/Images/piano.PNG"))
fenetre.blit(fond, (0,0))
pygame.display.flip()

def main ():
    instrument_choisi = pygame.K_F1
    continuer = True
    while continuer:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                continuer = False
          
            if event.type == KEYDOWN:
                if event.key in instruments:
                    instrument_choisi = event.key
                    print("Choix de l'instrument :", instruments[instrument_choisi]["nom"])
                elif event.key in instruments[instrument_choisi]["mapping"]:
                    instruments[instrument_choisi]["mapping"][event.key].play()
                
            if event.type == KEYUP:
                if event.key in instruments[instrument_choisi]["mapping"]:
                    instruments[instrument_choisi]["mapping"][event.key].stop()
                    
            if instruments [instrument_choisi] ["mapping"]== guitare_electrique :
                fond = pygame.image.load(os.path.join("/home/utilisateur/ISN/projet-bac-sampler/src/Images/guitare_electrique.jpg"))
                fenetre.blit(fond, (0,0))
                pygame.display.flip()
                
            if instruments [instrument_choisi] ["mapping"]== piano_synth :
                fond = pygame.image.load(os.path.join("/home/utilisateur/ISN/projet-bac-sampler/src/Images/piano.PNG"))
                fenetre.blit(fond, (0,0))
                pygame.display.flip()
                
            if instruments [instrument_choisi] ["mapping"]== guitare_acoustique :
                fond = pygame.image.load(os.path.join("/home/utilisateur/ISN/projet-bac-sampler/src/Images/piano.PNG"))
                fenetre.blit(fond, (0,0))
                pygame.display.flip()

            if instruments [instrument_choisi] ["mapping"]== flute_a_bec :
                fond = pygame.image.load(os.path.join("/home/utilisateur/ISN/projet-bac-sampler/src/Images/piano.PNG"))
                fenetre.blit(fond, (0,0))
                pygame.display.flip()                
                


if __name__ == "__main__":
    main()
